<?php
	if(!isset($_GET["id"])) {
		header("location: index.php?pagina=eventos");
	}
?>
<!-- Section -->
<section class="page-section">
    <div class="container relative">
        
        <h2 class="section-title align-center mb-70 mb-sm-40">
            1ER CAMPEONATO DE MOUNTAINBIKE COPA CERRO CHENA XCO
        </h2>
        
        <!-- Row -->
        <div class="row">
            
            <!-- Col -->
            
            <div class="col-sm-12">
                
                <!-- Nav Tabs -->
                <div class="align-center">
                    <ul class="nav nav-tabs tpl-minimal-tabs animate">
                        
                        <li class="active">
                            <a href="#carrera" data-toggle="tab">La Carrera</a>
                        </li>
                        
                        <li>
                            <a href="#horarios" data-toggle="tab">Horarios</a>
                        </li>
                        <li>
                            <a href="#resultados" data-toggle="tab">Resultados</a>
                        </li>
                        <li>
                            <a href="#fotos" data-toggle="tab">Fotos</a>
                        </li>
                    </ul>
                </div>
                <!-- Fin Nav Tabs -->
                
                <!-- Tabs -->
                <div class="tab-content tpl-minimal-tabs-cont align-center section-text">
                    <!-- Tab Carreras -->
                    <div class="tab-pane fade in active" id="carrera">
                    	<br>
                    	<div class="row">
                        	<div class="col-md-12">
                            	<a id="basePdf" href="images/carreras/1/bases/Bases-copa-cerro-Chena-2015.pdf"></a>
                            </div>
                        </div>                	
                    </div>
                    <!-- Fin Tab Carreras -->                    
                    
                    <!-- Tab Horarios -->
                    <div class="tab-pane fade" id="horarios">
                        <div class="col-md-3"></div>
                        <div class="col-md-6" style="text-align: left">
                            <br>
                            <h6 style="text-align: center">Horarios de la Carrera</h6>
                            <br>
                            <p>
                                <strong>09:30 Hrs 1ra Largada:</strong>
                                    <ul>
                                        <li>Intermedia</li>
                                        <li>Novicios Varones y Damas</li>
                                        <li>Juvenil</li>
                                        <li>Infantil</li>
                                    </ul>
                            </p>
                            <p>
                                    <strong>11:00 Hrs 2da Largada:</strong>
                                    <ul>
                                        <li>Master A1 y A2 / Master A Damas</li>
                                        <li>Master B1 y B2 / Master B Damas</li>
                                        <li>Master C1 y C2 / Master C Damas</li>
                                    </ul>
                            </p>
                            <p>
                                    <strong>12:30 Hrs 3era Largada (Circuito Niños):</strong>
                                    <ul>
                                        <li>Promocionales A</li>
                                        <li>Promocionales B</li>
                                    </ul>
                            </p>
                            <p>
                                    <strong>13:30 Hrs 4ta Largada:</strong>
                                    <ul>
                                        <li>Elite Varones y Damas</li>
                                        <li>Sub 23 Varones y Damas</li>
                                        <li>Junior Varones y Damas</li>
                                        <li>Amateur Damas / Amateur Varones</li>
                                    </ul>
                            </p>
                        </div>
                    	<div class="col-md-3"></div>
                    </div>
                    <!-- Fin Tab Horarios -->
                                        
                    <!-- Tab Resultados -->
                    <div class="tab-pane fade" id="resultados">
                    	<br>
                    	<div class="row">
                        	<div class="col-md-12">
                            	<a id="basePdf2" href="images/carreras/1/resultados/resultados.pdf"></a>
                            </div>
                        </div>
                    </div>
                    <!-- Fin Tab Resultados -->
                    
                    <!-- Tab Fotos -->
                    <div class="tab-pane fade" id="fotos">
                    	<h3>Puedes revisar el album completo siguiendo el siguiente enlace <a href="https://www.facebook.com/media/set/?set=a.879488382098528.1073741849.514112595302777&type=1&l=44be2a4563" target="_blank"> Haz Clic aquí </a></h3>
                    </div>
                    <!-- Fin Tab Fotos -->
                </div>
                <!-- Fin Tabs -->
            </div>
            
            <!-- Fin Col -->                        
            
        </div>
        <!-- Fin Row -->
        
    </div>
</section>
<!-- Fin Section -->

<script type="text/javascript">
	$(document).ready(function() {
        $("#basePdf").media({width: "100%", height:600}); // PDF
        $("#basePdf2").media({width: "100%", height:600}); // PDF
    });
</script>