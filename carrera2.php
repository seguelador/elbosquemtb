﻿<?php
	if(!isset($_GET["id"])) {
		header("location: index.php?pagina=eventos");
	}
?>
<!-- Section -->
<section class="page-section">
    <div class="container relative">
        
        <h2 class="section-title align-center mb-70 mb-sm-40">
            II XCO Cerro Chena
        </h2>
        
        <!-- Row -->
        <div class="row">
            
            <!-- Col -->
            
            <div class="col-sm-12">
                
                <!-- Nav Tabs -->
                <div class="align-center">
                    <ul class="nav nav-tabs tpl-minimal-tabs animate">
                        
                        <li class="active">
                            <a href="#carrera" data-toggle="tab">La Carrera</a>
                        </li>
                        <li>
                            <a href="#inscripcion" data-toggle="tab">Inscripción</a>
                        </li>
                        <li>
                            <a href="#horarios" data-toggle="tab">Horarios</a>
                        </li>
                        <li>
                            <a href="#resultados" data-toggle="tab">Resultados</a>
                        </li>
                        <li>
                            <a href="#fotos" data-toggle="tab">Fotos</a>
                        </li>
                    </ul>
                </div>
                <!-- Fin Nav Tabs -->
                
                <!-- Tabs -->
                <div class="tab-content tpl-minimal-tabs-cont align-center section-text">
                    <!-- Tab Carreras -->
                    <div class="tab-pane fade in active" id="carrera">
                    	<br>
                    	<div class="row">
                        	<div class="col-md-12">
                            	<a id="basePdf" href="images/carreras/2/bases/Bases II CXO.pdf"></a>
                            </div>
                        </div>                	
                    </div>
                    <!-- Fin Tab Carreras -->     

                    <!-- Tab Carreras -->
                    <div class="tab-pane fade" id="inscripcion">
                    	<br>
                    	<div class="row">
                        	<div class="col-md-3"></div>
                        	<div class="col-md-6">
                        		<form id="formInscripcion" class="form">
                        			<div class="form-group">
                        				<input type="text" class="form-control" id="txtNombre" name="txtNombre" placeholder="Ingrese su Nombre Completo">
                        			</div>
                        			<div class="form-group">
                        				<input type="text" class="form-control" id="txtRut" name="txtRut" placeholder="Ingrese su Rut">
                        			</div>
                        			<div class="form-group">
                        				<select class="form-control" id="selectCategoria" name="selectCategoria">
                        					<option selected disabled>Seleccione una categoria</option>
                        					<option value="Infantil">Infantil</option>
                        					<option value="Juvenil">Juvenil</option>
                        					<option value="Intermedia">Intermedia</option>
                        					<option value="Junior">Junior</option>
                        					<option value="Sub 23">Sub 23</option>
                        					<option value="Elite">Elite</option>
                        					<option value="Master A Damas">Master A Damas</option>
                        					<option value="Master A-1">Master A-1</option>
                        					<option value="Master A-2">Master A-2</option>
                        					<option value="Master B Damas">Master B Damas</option>
                        					<option value="Master B-1">Master B-1</option>
                        					<option value="Master B-2">Master B-2</option>
                        					<option value="Master C Damas">Master C Damas</option>
                        					<option value="Master C-1">Master C-1</option>
                        					<option value="Master C-2">Master C-2</option>
                        					<option value="Novicio">Novicio</option>
                        					<option value="Amateur">Amateur</option>
								<option value="Ciclo Cross">Ciclo Cross</option>
                        				</select>
                        			</div>
                        			<div class="form-group">
                        				<input type="date" class="form-control" id="txtFecha" name="txtFecha">
                        			</div>
                                    <div class="form-group">
                                        <input type="text" class="form-control" id="txtEmail" name="txtEmail" placeholder="Ingrese su Email">
                                    </div>
                        			<div class="form-group">
                        				<input type="file" class="form-control" id="fileComprobante" name="fileComprobante">
                        			</div>
                        			<button id="btnEnviar" class="submit_btn btn btn-mod btn-color btn-large" type="button">Enviar</button>
                        		</form>
                        	</div>
                        </div>                	
                    </div>
                    <!-- Fin Tab Carreras -->                 
                    
                    <!-- Tab Horarios -->
                    <div class="tab-pane fade" id="horarios">
                        <div class="col-md-3"></div>
                        <div class="col-md-6" style="text-align: left">
                            <br>
                            <h6 style="text-align: center">Horarios de la Carrera</h6>
                            <br>
                            <p>
                                <strong>09:30 Hrs 1ra Largada:</strong>
                                    <ul>
                                        <li>Intermedia</li>
                                        <li>Novicios Varones y Damas</li>
                                        <li>Juvenil</li>
                                        <li>Infantil</li>
                                    </ul>
                            </p>
                            <p>
                                    <strong>11:00 Hrs 2da Largada:</strong>
                                    <ul>
                                        <li>Master A1 y A2 / Master A Damas</li>
                                        <li>Master B1 y B2 / Master B Damas</li>
                                        <li>Master C1 y C2 / Master C Damas</li>
                                    </ul>
                            </p>
                            <p>
                                    <strong>12:30 Hrs 3era Largada (Circuito Niños):</strong>
                                    <ul>
                                        <li>Promocionales A</li>
                                        <li>Promocionales B</li>
                                    </ul>
                            </p>
                            <p>
                                    <strong>13:30 Hrs 4ta Largada:</strong>
                                    <ul>
                                        <li>Elite Varones y Damas</li>
                                        <li>Sub 23 Varones y Damas</li>
                                        <li>Junior Varones y Damas</li>
                                        <li>Amateur Damas / Amateur Varones</li>
                                    </ul>
                            </p>
                        </div>
                    	<div class="col-md-3"></div>
                    </div>
                    <!-- Fin Tab Horarios -->
                                        
                    <!-- Tab Resultados -->
                    <div class="tab-pane fade" id="resultados">
                    	<br>
                    	<div class="row">
                        	<div class="col-md-12">
                            	<h3>Los resultados estarán disponibles una vez finalice la carrera.</h3>
                            </div>
                        </div>
                    </div>
                    <!-- Fin Tab Resultados -->
                    
                    <!-- Tab Fotos -->
                    <div class="tab-pane fade" id="fotos">
                    	<h3>Las fotografías serán publicadas una vez finalice la carrera.</h3>
                    </div>
                    <!-- Fin Tab Fotos -->
                </div>
                <!-- Fin Tabs -->
            </div>
            
            <!-- Fin Col -->                        
            
        </div>
        <!-- Fin Row -->
        
    </div>
</section>
<!-- Fin Section -->

<script type="text/javascript">
	$(document).ready(function() {
        $("#basePdf").media({width: "100%", height:600}); // PDF
        $("#basePdf2").media({width: "100%", height:600}); // PDF

        $("#txtFecha").on("focus", function(){
            alert("Ingrese fecha de nacimiento del corredor");
        })

        $("#btnEnviar").click(function(){
            $.ajax({
                beforeSend: function(){
                    $("#btnEnviar").text("Enviando...");
                },
                url: "panel/scripts/script-carrera-enviar-comprobante-mail.php",
                type: "POST",
                data: new FormData($("#formInscripcion")[0]),
                contentType: false,
                cache: false,
                processData:false,        
                success: function(response){
                    alert(response);
                }
            }).done(function(){
                $("#btnEnviar").text("Enviar");
            });

        });
        
    });
</script>