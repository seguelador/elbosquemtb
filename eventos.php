<!-- Section -->
<section class="page-section">
    <div class="container relative">
        
        <div class="row">
            
            <!-- Content -->
            <div class="col-sm-10 col-sm-offset-1">

                <!-- Post -->
                <div class="blog-item">
                    
                    <!-- Fecha -->
                    <div class="blog-item-date">
                        <span class="date-num">13</span>Dic
                    </div>
                    <!-- Post Title -->
                    <h2 class="blog-item-title"><a href="index.php?pagina=carrera2&id=1">II XCO Cerro Chena</a></h2>
                                        
                    <!-- Imagen -->
                    <div class="blog-media">
                        <a href="index.php?pagina=carrera2&id=1"><img src="images/blog/2/AFICHE MTB.jpg" alt="" /></a>
                    </div>
                    
                    <!-- Text Intro -->
                    <div class="blog-item-body">
                        <p>
                            Te invitamos a participar de nuestra segunda versión! No te lo pierdas.
                        </p>
                    </div>
                    
                    <!-- Ver mas Link -->
                    <div class="blog-item-foot">
                        <a href="index.php?pagina=carrera2&id=1" class="btn btn-mod btn-color  btn-small">Ver más <i class="fa fa-angle-right"></i></a>
                    </div>
                    
                </div>
                <!-- Fin Post -->
                                
                <!-- Post -->
                <div class="blog-item">
                    
                    <!-- Fecha -->
                    <div class="blog-item-date">
                        <span class="date-num">15</span>Jul
                    </div>
                    <!-- Post Title -->
                    <h2 class="blog-item-title"><a href="index.php?pagina=carrera&id=1">1er Campeonato de Mountainbike Copa Cerro Chena XCO</a></h2>
                                        
                    <!-- Imagen -->
                    <div class="blog-media">
                        <a href="index.php?pagina=carrera&id=1"><img src="images/blog/1/AFICHE MTB.jpg" alt="" /></a>
                    </div>
                    
                    <!-- Text Intro -->
                    <div class="blog-item-body">
                        <p>
                            Ya tenemos fecha! Están todos cordialmente invitados!
                        </p>
                    </div>
                    
                    <!-- Ver mas Link -->
                    <div class="blog-item-foot">
                        <a href="index.php?pagina=carrera&id=1" class="btn btn-mod btn-color  btn-small">Ver más <i class="fa fa-angle-right"></i></a>
                    </div>
                    
                </div>
                <!-- Fin Post -->
                
                <!-- Paginacion descomentar en caso de uso
                <div class="pagination">
                    <a href="#"><i class="fa fa-angle-left"></i></a>
                    <a href="#" class="active">1</a>
                    <a href="#">2</a>
                    <a href="#">3</a>
                    <a class="no-active">...</a>
                    <a href="#">9</a>
                    <a href="#"><i class="fa fa-angle-right"></i></a>
                </div>
                <!-- Fin Paginacion -->
                
            </div>
            <!-- Fin Content -->
            
        </div>
        
    </div>
</section>
<!-- End Section -->