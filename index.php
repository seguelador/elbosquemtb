<!DOCTYPE html>
<html>
<head>
        <title>El Bosque MTB</title>
        <meta name="description" content="El club de ciclismo de montaña El Bosque MTB, se formó en marzo del año 2010, concretando la idea de un grupo de amigos que desarrollaban esta actividad desde la infancia y además compartían intereses en común, como la cercanía por la naturaleza, el hábito deportivo y el espíritu de superación a nivel personal y colectivo.
Es así como se fue formalizando una iniciativa deportiva, a través de un club que desempeña 3 grandes áreas; Área competitiva, Área formativa y Área social-recreativa. Lo anterior tiene como principio fundamental ser una institución integradora para aquellas personas que tengan deseos y el compromiso de participar de las actividades que desarrollamos.">
        <meta name="keywords" content="mountainbike, mtb, xco, xcm, cross country, el bosque, ciclismo, montaña, amigos, club, club el bosque mtb">
        <meta charset="utf-8">
        <meta name="author" content="Explosion.cl">
        <!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
        
        <!-- Favicons -->
       <!-- <link rel="shortcut icon" href="images/favicon.png">
        <link rel="apple-touch-icon" href="images/apple-touch-icon.png">
        <link rel="apple-touch-icon" sizes="72x72" href="images/apple-touch-icon-72x72.png">
        <link rel="apple-touch-icon" sizes="114x114" href="images/apple-touch-icon-114x114.png">-->
        
        <!-- CSS -->
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <link rel="stylesheet" href="css/style.css">
        <link rel="stylesheet" href="css/style-responsive.css">
        <link rel="stylesheet" href="css/animate.min.css">
        <link rel="stylesheet" href="css/vertical-rhythm.min.css">
        <link rel="stylesheet" href="css/owl.carousel.css">
        <link rel="stylesheet" href="css/magnific-popup.css">
        <link rel="stylesheet" href="css/fileinput.min.css">
        
        <link rel="stylesheet" href="css/colors/green.css">
        
        <script type="text/javascript" src="js/jquery-1.11.2.min.js"></script>

        <style>
        .logo-item{
            cursor: pointer;
        }
        </style>
        
    </head>
    
    <body class="appear-animate">
        
                
        <div class="page" id="top">
            
            <!-- Nav -->
            <nav class="main-nav white stick-fixed">
                <div class="full-wrapper relative clearfix">
                    <div class="nav-logo-wrap local-scroll">
                        <a href="http://elbosquemtb.cl" class="logo">
                            <img src="images/logo.png" alt="" />
                        </a>
                    </div>
                    <div class="mobile-nav">
                        <i class="fa fa-bars"></i>
                    </div>
                    <!-- Main Menu -->
                    <div class="inner-nav desktop-nav">
                        <ul class="clearlist scroll-nav local-scroll">
                            <li class="active"><a href="index.php#inicio">Inicio</a></li>
                            <li><a href="index.php#nosotros">Nosotros</a></li>
                            <li><a href="index.php#equipo">Equipo</a></li>
                            <li><a href="index.php?pagina=eventos">Eventos</a></li>
                            <li><a href="index.php#contacto">Contacto</a></li>                            
                        </ul>
                    </div>
                    <!-- Fin Main Menu -->
                </div>
            </nav>
            <!-- Fin Nav -->
            
            <?php
                if(isset($_GET["pagina"])) {
                    $pagina = $_GET["pagina"];
                    include($pagina.".php");		
                } else {
                    ?>
                    <?php
                    include("inicio.php");
                }			
            ?>
            
            <!-- Footer -->
            <footer class="small-section bg-color footer pb-60">
                <div class="container">
                    
                    <!-- Facebook-->
                    <div class="footer-social-links mb-60 mb-xs-40" style="color:#4caf50;">
                        <a href="http://www.facebook.com/elboskemtb" title="Facebook" target="_blank"><i class="fa fa-facebook"></i></a>
                    </div>
                    <!-- Fin Facebook -->  
                    
                    <div class="footer-text">
                        
                        <!-- Copyright -->
                        <div class="footer-copy" style="color:#fff;">
                            &copy; El Bosque MTB
                        </div>
                        <!-- End Copyright -->
                        
                        <div class="footer-made">
                            <a href="http://explosion.cl" target="_blank">Desarrollo Web Explosion.cl</a>
                        </div>
                        
                    </div>
                    
                 </div>
                 
                 
                 <!-- Top Link -->
                 <div class="local-scroll">
                     <a href="#top" class="link-to-top"><i class="fa fa-caret-up"></i></a>
                 </div>
                 <!-- Fin Top Link -->
                 
            </footer>
            <!-- Fin Foter -->
        
        
        </div>
        <!-- Fin Page Wrap -->
        
        
        <!-- JS -->        
        <script type="text/javascript" src="js/jquery.easing.1.3.js"></script>
        <script type="text/javascript" src="js/bootstrap.min.js"></script>        
        <script type="text/javascript" src="js/SmoothScroll.js"></script>
        <script type="text/javascript" src="js/jquery.scrollTo.min.js"></script>
        <script type="text/javascript" src="js/jquery.localScroll.min.js"></script>
        <script type="text/javascript" src="js/jquery.viewport.mini.js"></script>
        <script type="text/javascript" src="js/jquery.countTo.js"></script>
        <script type="text/javascript" src="js/jquery.appear.js"></script>
        <script type="text/javascript" src="js/jquery.sticky.js"></script>
        <script type="text/javascript" src="js/jquery.parallax-1.1.3.js"></script>
        <script type="text/javascript" src="js/jquery.fitvids.js"></script>
        <script type="text/javascript" src="js/owl.carousel.min.js"></script>
        <script type="text/javascript" src="js/isotope.pkgd.min.js"></script>
        <script type="text/javascript" src="js/imagesloaded.pkgd.min.js"></script>
        <script type="text/javascript" src="js/jquery.magnific-popup.min.js"></script>
        <script type="text/javascript" src="js/wow.min.js"></script>
        <script type="text/javascript" src="js/masonry.pkgd.min.js"></script>
        <script type="text/javascript" src="js/jquery.simple-text-rotator.min.js"></script>
        <script type="text/javascript" src="js/all.js"></script>
        <script type="text/javascript" src="js/contact-form.js"></script>
        <script type="text/javascript" src="js/jquery.ajaxchimp.min.js"></script>    
        <script type="text/javascript" src="js/jquery.media.js"></script>     
        <script type="text/javascript" src="js/fileinput.min.js"></script>   
        <script type="text/javascript" src="js/fileinput_locale_es.js"></script>      
        <script type="text/javascript" src="js/jquery.validate.min.js"></script>   
        
        <!--[if lt IE 10]><script type="text/javascript" src="js/placeholder.js"></script><![endif]-->
        
    </body>
</html>
