<!-- Inicio Section -->

<head>
<meta http-equiv="Content-type" content="text/html; charset=utf-8" />
<style type="text/css">
.auto-style1 {
	font-family: Arial, Helvetica, sans-serif;
}
</style>
</head>

<section class="home-section bg-dark parallax-3" data-background="images/full-width-images/DSC_1132.jpg" id="inicio">
    <div class="js-height-full relative">
        
        <span class="auto-style1">
        
        <!-- Content -->
        </span>
        <div class="home-content">
            <div class="home-text">
                
                
                <div class="hs-line-2 mt-0 mb-40 mb-xs-20">
                 
                </div>
                
                <div class="auto-style1">
                    <a href="http://www.paysport.cl/races/47-xcm-san-bernardo--cerro-chena/" class="btn btn-mod btn-w btn-large hidden-xs">
					Inscribete Aqui</a>
                </div>
                
            </div>
        </div>
        <span class="auto-style1">
        <!-- Fin Content -->
        
        <!-- Scroll -->
        </span>
        <div class="local-scroll">
            <a href="#nosotros" class="scroll-down"><i class="fa fa-angle-down scroll-down-icon"></i></a>
        </div>
        <span class="auto-style1">
        <!-- Fin Scroll -->
        
    	</span>
        
    </div>
</section>
<span class="auto-style1">
<!-- Fin Inicio Section -->

<!-- Nosotros Section -->
</span>
<section class="page-section" id="nosotros">
    <div class="container relative">                    
        <div class="row">
            <div class="col-md-8 col-md-offset-2 align-center">
                
                <span class="auto-style1">
                
                <!-- Titulos Section -->
                </span>
                <h1 class="auto-style1">
                    Club de Ciclismo de Montaña El Bosque MTB                 </h1>
                <h2 class="auto-style1">
                    Nosotros
                </h2>
                <span class="auto-style1">
                <!-- Fin Titulos Section -->
                
                </span>
                
                <div class="section-line mb-50 mb-xs-30"></div>
                
                <span class="auto-style1">
                
                <!-- Descripcion -->
                </span>
                <div class="section-text mb-80 mb-xs-50">
                    <p class="auto-style1">El club de ciclismo de montaña  <strong>
					El Bosque MTB</strong>, se forma en marzo del año 2010, 
					concretando la idea de un grupo de amigos que desarrollaban 
					esta actividad desde la infancia y además compartían 
					intereses en común, como la cercanía por la naturaleza, el 
					ámbito deportivo y el espíritu de superación a nivel 
					personal y colectivo. . Lo anterior tiene como principio 
					fundamental ser una institución integradora para aquellas 
					personas que tengan deseos y el compromiso de participar de 
					las actividades que desarrollamos.</p>
<p class="auto-style1">Geográficamente, el Club El Bosque MTB tiene como patio 
de entrenamiento los más recónditos lugares del cerro Chena en la comuna de San 
Bernardo, lugar donde se desarrollan las áreas Competitivas (Entrenamiento 
para carreras de XCO y XCM) y Formativas (escuela de MTB para principiantes).</p>
<p class="auto-style1">En cuanto al área social-recreativa, se pueden 
mencionar diversas charlas educativas que impartimos a la comunidad, como por 
ejemplo, nutrición para deportistas, mecánica básica y teorí­a del 
ciclismo de montañaa. También se realizan cicletadas familiares, instancias 
en las cuales promovemos el uso de la bicicleta y el hábito saludable, entre 
otras iniciativas.</p>
                </div>
                <span class="auto-style1">
                <!-- Fin descripcion -->
                
            	</span>
                
            </div>
        </div>
        
        <span class="auto-style1">
        
        <!-- Grid -->
        </span>
        <div class="row multi-columns-row service-grid">
            
            <div class="col-sm-6 col-md-4 col-lg-4">
                <div class="service-item">
                    <div class="service-icon color">
                        <span class="fa fa-dot-circle-o"></span>
                    </div>
                    <h3 class="auto-style1">Misión</h3>
                    <span class="auto-style1">El Club El Bosque MTB tiene como 
					misión fundamental, ser una institución de niños, 
					jóvenes y adultos conectados con la naturaleza, 
					fomentando el respeto por los demás y por el 
					espíritu de crecimiento personal. Sin olvidar nuestro 
					horizonte deportivo y competitivo, el cual nos motiva a 
					entrenar y superar nuestros lí­mites para obtener grandes 
					logros deportivos. Este club deportivo también es un 
					espacio promotor de factores protectores frente al 
					sedentarismo y el consumo de drogas entre otros.
                	</span>
                </div>
            </div>
            
            <div class="col-sm-6 col-md-4 col-lg-4">
                <div class="service-item">
                    <div class="service-icon color">
                        <span class="icon-presentation"></span>
                    </div>
                    <h3 class="auto-style1">Visión</h3>
                    <span class="auto-style1">Pretendemos ser uno de los clubes 
					de ciclismo de montaña destacados de Chile, con la 
					capacidad de ser formadores de nuevos talentos deportivos, 
					los cuales representen al club en los escenarios más 
					importantes del ciclismo a nivel nacional e internacional, 
					sin olvidar nunca, el compromiso por las áreas formativas 
					y sociales- recreativas.
                </span>
                </div>
            </div>
            
            <div class="col-sm-6 col-md-4 col-lg-4">
                <div class="service-item">
                    <div class="service-icon color">
                        <span class="icon-linegraph"></span>
                    </div>
                    <h3 class="auto-style1">Objetivo</h3>
                    <span class="auto-style1">Promover la actividad 
					bá­sica y la vida saludable en niños, jóvenes y 
					adultos de la comuna de El Bosque y sus alrededores, a 
					través de la práctica del ciclismo de montaña en 
					sus modalidades de XCM y XCO, asiÃ‚Â­ como también, a 
					través de escuelas formativas de MTB y actividades 
					recreativas.
                	</span>
                </div>
            </div>
            
        </div>
        <span class="auto-style1">
        <!-- Fin Grid -->
    	</span>
    </div>
</section>
<span class="auto-style1">
<!-- Fin Nosotros Section -->
        
<!-- Equipo Section -->
</span>
<section class="page-section" id="equipo">
    <div class="container relative"> 
    
        <div class="row">
            <div class="col-md-8 col-md-offset-2 align-center">
                <span class="auto-style1">
                <!-- Head Section -->
                </span>
                <h2 class="auto-style1">
                    Directiva El Bosque MTB
                </h2>
                <span class="auto-style1">
                <!-- Fin Head Section -->
                </span>
                <div class="section-line mb-50 mb-xs-30"></div>
                
                <span class="auto-style1">
                
                <!-- Descripcion Section -->
                </span>
                <div class="auto-style1">
                    Conoce a nuestra Directiva 2016
                </div>
                <span class="auto-style1">
                <!-- Fin Descripcion Section -->
                
            	</span>
                
            </div>
        </div>
           
        <span class="auto-style1">
           
        <!-- Team Grid -->
        </span>
        <div class="row multi-columns-row">
            
            <span class="auto-style1">
            
            <!-- item relleno -->
            </span>
            <div class="col-sm-1 col-md-1 col-lg-1"></div>
            <span class="auto-style1">
            <!-- fin item relleno -->
            <!-- Team Item -->
            </span>
            <div class="col-sm-6 col-md-2 col-lg-2 mb-md-30 wow fadeInUp" data-wow-delay="0.3s">
                <div class="team-item">
                    
                    <div class="team-item-image">
                        
                        <img src="images/team/felipe.jpg" alt="" class="auto-style1" />
                        
                        <div class="team-item-detail">
                            
                            <h4 class="auto-style1">Presidente</h4>
                            
                        </div>
                    </div>
                    
                    <div class="team-item-descr">
                        
                        <div class="auto-style1">
                            Felipe Poblete
                        </div>
                        
                        <div class="auto-style1">
                            Presidente
                        </div>
                        
                    </div>
                    
                </div>
            </div>
            <span class="auto-style1">
            <!-- Fin Team Item -->
            
            <!-- Team Item -->
            </span>
            <div class="col-sm-6 col-md-2 col-lg-2 mb-md-30 wow fadeInUp" data-wow-delay="0.3s">
                <div class="team-item">
                    
                    <div class="team-item-image">
                        
                        <img src="images/team/felipe2.jpg" alt="" class="auto-style1" />
                        
                        <div class="team-item-detail">
                            
                            <h4 class="auto-style1">Tesorero</h4>
                            
                        </div>
                    </div>
                    
                    <div class="team-item-descr">
                        
                        <div class="auto-style1">
                            Felipe Sepulveda
                        </div>
                        
                        <div class="auto-style1">
                            Tesorero
                        </div>
                        
                    </div>
                    
                </div>
            </div>
            <span class="auto-style1">
            <!-- Fin Team Item -->
            
           
            
            <!-- item relleno -->
            </span>
            <div class="col-sm-1 col-md-1 col-lg-1"></div>
            <span class="auto-style1">
            <!-- fin item relleno -->
        	</span>
        </div>
        <span class="auto-style1">
        <!-- Fin Equipo Grid -->
        
    	</span>
        
    </div>
</section>
<span class="auto-style1">
<!-- Fin Equipo Section -->

<!-- Separador Section -->
</span>
<section class="small-section bg-color-alfa-90 bg-scroll" data-background="images/full-width-images/section-bg-4.jpg">
    <div class="container relative">
        
        <div class="row">
            <div class="col-md-8 col-md-offset-2 align-center">
                
                <h1 class="auto-style1">¿Deseas Unirte?</h1>
                
                <h2 class="auto-style1">Si deseas compartir salidas o unirte al 
				club, no dudes en contactarnos para obtener mayor 
				información.</h2>
                
                <div class="auto-style1">
                    <a href="#contacto" class="btn btn-mod btn-border-w btn-large">
					Contáctanos</a>
                </div>
                
            </div>
        </div>
        
    </div>
</section>
<span class="auto-style1">
<!-- Fin separador Section -->


<!-- Logos Section -->
</span>
<section class="page-section" id="equipo">
    <div class="container relative"> 
        <div class="row">
            <div class="col-md-8 col-md-offset-2 align-center">
                <span class="auto-style1">
                <!-- Head Section -->
                </span>
                <h2 class="auto-style1">
                    Auspiciadores y Patrocinadores
                </h2>
                <span class="auto-style1">
                <!-- Fin Head Section -->
                </span>
                <div class="section-line mb-50 mb-xs-30"></div>
                
                <span class="auto-style1">
                
                <!-- Descripcion Section -->
                </span>
                <div class="auto-style1">
                    Conoce a nuestros auspiciadores y patrocinadores, que 
					permiten que nuestro club y el mundo del ciclismo siga 
					creciendo.
                </div>
                <span class="auto-style1">
                <!-- Fin Descripcion Section -->
                
            	</span>
                
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                
                <div class="small-item-carousel black owl-carousel mb-0 animate-init" data-anim-type="fade-in-right-large" data-anim-delay="100">
                    
                    <span class="auto-style1">
                    
                    <!-- Logo Item -->
                    </span>
                    <div class="logo-item">
                        <img src="images/clients-logos/barrilito_rojo.png" width="100" height="100" alt="" data-link="https://www.facebook.com/distribuidora.barrilito?fref=ts" class="auto-style1" /><span class="auto-style1">
						</span>
                    </div>
                    <span class="auto-style1">
                    <!-- Fin Logo Item -->
                    
                    <!-- Logo Item -->
                    </span>
                    <div class="logo-item">
                        <img src="images/clients-logos/pamacri.png" width="100" height="100" alt="" data-link="https://www.facebook.com/pamacri.losmorros" class="auto-style1" /><span class="auto-style1">
						</span>
                    </div>
                    <span class="auto-style1">
                    <!-- Fin Logo Item -->
                    
                    <!-- Logo Item -->
                    </span>
                    <div class="logo-item">
                        <img src="images/clients-logos/crgym.png" width="100" height="100" alt="" data-link="https://www.facebook.com/cyros.gym?fref=ts" class="auto-style1" /><span class="auto-style1">
						</span>
                    </div>
                    <span class="auto-style1">
                    <!-- Logo Item -->
                    </span>
                    <div class="logo-item">
                        <img src="images/clients-logos/lgmbikes-logo-1442276481.jpg" width="350" height="92" alt="" data-link="http://www.LGMbikes.cl" class="auto-style1"><span class="auto-style1">
						</span>
                    </div>
                    <span class="auto-style1">
                    <!-- Fin Logo Item -->
                    <!-- Fin Logo Item -->
                    
                    <!-- Logo Item -->
                    </span>
                    <div class="logo-item">
                        <img src="images/clients-logos/l3MHsmNP.png" width="100" height="100" alt="" data-link="https://www.facebook.com/munielbosque?fref=ts" class="auto-style1" /><span class="auto-style1">
						</span>
                    </div>
                    <span class="auto-style1">
                    <!-- Fin Logo Item -->
                    
                    <!-- Logo Item -->
                    </span>
                    <div class="logo-item">
                        <img src="images/clients-logos/logo-aprochile.png" width="100" height="100" alt="" data-link="http://www.aprochile.cl/" class="auto-style1" /><span class="auto-style1">
						</span>
                    </div>
                    <span class="auto-style1">
                    <!-- Fin Logo Item -->

                    <!-- Logo Item -->
                    </span>
                    <div class="logo-item">
                        <img src="images/clients-logos/gas.png" width="80" height="80" alt="" class="auto-style1" /><span class="auto-style1">
						</span>
                    </div>
                    <span class="auto-style1">
                    <!-- Fin Logo Item -->

                    <!-- Logo Item -->
                    </span>
                    <div class="logo-item">
                        <img src="images/clients-logos/bioenergy.png" width="80" height="80" alt="" data-link="https://www.facebook.com/pages/Bioenergysport/1618392051768788?fref=ts" class="auto-style1" /><span class="auto-style1">
						</span>
                    </div>
                    <span class="auto-style1">
                    <!-- Fin Logo Item -->
                    <!-- Logo Item -->
                    </span>
                    <div class="logo-item">
                        <img src="images/clients-logos/logopedalea.png" width="214" height="59" alt="" data-link="https://www.facebook.com/profile.php?id=100008529607192&amp;fref=ts"><span class="auto-style1">
						</span>
                    </div>
                    <span class="auto-style1">
                    <!-- Fin Logo Item -->

                    
                                                    
                	</span>
                    
                                                    
                </div>
                    
             </div>
         </div>
        
     </div>
</section>
<span class="auto-style1">
<!-- Fin Logos -->


<!-- Contacto Section -->
</span>
<section class="page-section" id="contacto">
    <div class="container relative">
        
        <div class="row">
            <div class="col-md-8 col-md-offset-2 align-center">
                
                <h2 class="auto-style1">Escrí­benos</h2>
                <div class="section-line mb-50 mb-xs-30"></div>
            </div>
        </div>                    
        
        <span class="auto-style1">                    
        
        <!-- Form Contacto -->                            
        </span>                            
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                
                <form class="form contact-form" id="contact_form" autocomplete="off">
                    <div class="clearfix">
                        
                        <div class="cf-left-col">
                            
                            <div class="form-group">
                                <input type="text" name="txtNombre" id="txtNombre" class="auto-style1" placeholder="Nombre"><span class="auto-style1">
								</span>
                            </div>
                            
                            <div class="form-group">
                                <input type="email" name="txtEmail" id="txtEmail" class="auto-style1" placeholder="Email"><span class="auto-style1">
								</span>
                            </div>
                            
                        </div>
                        
                        <div class="cf-right-col">
                            
                            <div class="form-group">                                            
                                <textarea name="txtMensaje" id="txtMensaje" class="auto-style1" style="height: 120px;" placeholder="Mensaje"></textarea><span class="auto-style1">
								</span>
                            </div>
                            
                        </div>
                        
                    </div>
                    
                    <div class="clearfix">
                        
                        <div class="cf-left-col">
                            
                            <span class="auto-style1">
                            
                            <!-- Tip -->                                        
                            </span>                                        
                            <div class="auto-style1">
                                <i class="fa fa-info-circle"></i> Todos los 
								campos son requeridos
                            </div>
                            
                        </div>
                        
                        <div class="cf-right-col">
                            
                            <span class="auto-style1">
                            
                            <!-- Enviar Button -->
                            </span>
                            <div class="align-right pt-10">
                                <button class="auto-style1" id="submit_btn">
								Enviar Mensaje</button>
                            </div>
                            
                        </div>
                        
                    </div>
                    
                    
                    <div id="result"></div>
                </form>
                
            </div>
        </div>
        <span class="auto-style1">
        <!-- Fin form Contacto -->
        
    	</span>
        
    </div>
</section>
<span class="auto-style1">
<!-- Fin Contacto Section -->
<script type="text/javascript">
$(document).ready(function(){
    $(".logo-item").click(function(){
        var link = $(this).children().attr("data-link");
        window.open(link, '_blank');
    });
});
</script></span>