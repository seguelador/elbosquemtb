/* ---------------------------------------------
Formulario de Contacto
 --------------------------------------------- */
$(document).ready(function(){
    $("#submit_btn").click(function(){
        
        //values
        var nombre = $("#txtNombre").val();
        var email = $("#txtEmail").val();
        var mensaje = $("#txtMensaje").val();
        
        //validacion simple, cambia de color el borde simplemente
        var proceed = true;
        if (nombre == "") {
            $("#txtNombre").css('border-color', '#e41919');
            proceed = false;
        }
        if (email == "") {
            $("#txtEmail").css('border-color', '#e41919');
            proceed = false;
        }
        
        if (mensaje == "") {
            $("#txtMensaje").css('border-color', '#e41919');
            proceed = false;
        }
        
        //Si todo esta ok continua
        if (proceed) {
            //datos
			$.ajax({
				beforeSend: function(){
					$("#submit_btn").text("ENVIANDO...").attr('disabled',true);					
				},
				url: "panel/scripts/script-contacto.php",
				type:"POST",
				data:{nom: nombre, ema: email, men: mensaje},
				success: function(response){
					if(response == 1) {
						output = '<div class="success">Se ha enviado el mensaje correctamente, nos pondremos en contacto contigo a la brevedad.</div>';
						$("#txtNombre").val("");
						$("#txtEmail").val("");
						$("#txtMensaje").val("");
					} else {
						output = '<div class="error">Se ha producido un error al intentar enviar el mensaje, intentalo nuevamente, disculpa los inconvenientes.</div>';
					}
					$("#result").hide().html(output).slideDown();
				}
			}).done(function(){
				$("#submit_btn").text("ENVIAR MENSAJE").attr('disabled',false);
			})
            
        }
        
        return false;
    });
    
    //resetea el contacto en keyup
    $("#contact_form input, #contact_form textarea").keyup(function(){
        $("#contact_form input, #contact_form textarea").css('border-color', '');
        $("#result").slideUp();
    });
    
});
